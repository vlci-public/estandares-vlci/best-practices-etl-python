**Índice**

[[_TOC_]]

# Buenas prácticas en las ETLs (General)

## Nomenclatura de una ETL

El nombre siempre será en **minúsculas** y seguirá la siguiente norma: [TIPOSW]\_[SERV]\_[ind]\_[DESCRIPCION]

**TIPOSW**

- etl - cuando es una ETL de Talend
- py - cuando es una ETL de Python
- sh - cuando es un script de unix.

**SERV**

El servicio del Ayuntamiento que proporciona los datos. Ejemplos:

- movilidad, calidadambiental, tributaria
  O bien:
- integracion: Si es una ETL técnica de la OTP. Por ejemplo etl loader.

**ind**

Opcional, lo pondremos cuando la ETL calcules Indicadores

**DESCRIPCION**

- Descripcion libre de la ETL.

Así por ejemplo:

- py_integracion_housekeeping: Es una ETL hecha en Python, técnica, no nos la pide ningún servicio. Hace housekeeping.
- etl_calidadambiental_ind_contaminacion: ETL hecha en Talend del Servicio de Calidad Ambiental, calcula Indicadores de contaminación.
- py_tesoreria_codigos_organicos: Es una ETL hecha en Python del Servicio de Tesorería, que inserta códigos orgánicos en bbdd.

## Envío de emails

En general se desaconseja enviar emails a no ser que la ETL acabe OK y aún así queramos indicar algo al usuario; en ese caso sí usaremos el envío de emails. En caso contrario, de acabar con error, siempre se mostrará a través de los logs un mensaje con una explicación amplia del error y qué hacer para recuperarse de ese error.

Por tanto, las ETLs enviarán emails sólo cuando haya que realizar alguna acción concreta por parte de la persona que reciba el email y la ETL esté previsto acabar sin error. En el caso de querer informar de algo y que la ETL acaba OK, se podrá realizar un envío de email y la acción concreta que la persona de soporte deba realizar deberá estar explicada en el cuerpo del email. Por ejemplo, si hay un X número de registros descartados y se envía un email, se debe explicar qué hacer. Dicho de otro modo: todos los emails que se envíen deben indicar cómo reaccionar ante el problema que se describe.

# Buenas prácticas en las ETLs de Python

## Telefónica

Seguir el manual de buenas prácticas de Telefónica [Buenas Practicas ETLs (Telefonica)](https://github.com/telefonicasc/etl-framework/blob/master/doc/best_practices.md)

## Ayuntamiento de Valencia

Este es el listado de buenas prácticas que usamos en las ETLs desarrolladas para el Ayuntamiento de Valencia. Recordar siempre esta frase, del libro Clean Code, de Robert C. Martin: "Una diferencia entre un programador inteligente y un programador profesional es que este último sabe que la claridad es lo que importa. Los profesionales usan sus poderes para hacer el bien y crean código que otros puedan entender."

### Variables

Respecto a las variables usadas dentro de un programa Python, definamos primero los diferentes estándares:

- lowercase
- lower_case_with_underscores
- UPPERCASE
- UPPER_CASE_WITH_UNDERSCORES
- CamelCase (or CapitalizedWords or CapWords). This is also sometimes known as StudlyCaps. Note: When using acronyms in CapWords, capitalize all the letters of the acronym. Thus HTTPServerError is better than HttpServerError.
- mixedCase (differs from CapitalizedWords by initial lowercase character!)

Dicho eso:

- [ ] Las variables se nombran con lower_case_with_underscores
- [ ] Las constantes se definen a nivel de módulo y usan UPPER_CASE_WITH_UNDERSCORES. Ejemplos: MAX_OVERFLOW and TOTAL.
- [ ] Las funciones siguen el mismo principio que las variables y se nombran con lower_case_with_underscores
- [ ] Los nombres de clases usan el CamelCase
- [ ] Las excepciones, al ser clases, también usan el CamelCase más el sufijo Error
- [ ] Para los métodos públicos de una clase, usar el mismo estándar de nombrado que el de las funciones: lower_case_with_underscores
- [ ] Si el método de la clase es privado, poner un underscore al principio del nombre

Más info al respecto [PEP 8 – Style Guide for Python Code](https://peps.python.org/pep-0008/#function-and-variable-names)

### Nombres de los objetos

Objetos se refiere a: variables, funciones, clases, métodos, nombres de ficheros.

- [ ] Siempre en castellano.
- [ ] Las nombres de los objetos siempre son autoexplictivos, no usar cosas como: `cont1`, `cont2` o `int d; //Día de la semana en número`.
- [ ] Evitar crear variables con exceso de información, por ejemplo incluir el tipo de una variable en el nombre en muchos casos es innecesario
- [ ] Usar siempre, nombres que se puedan pronunciar, y nombres que se puedan buscar, además que la longitud de un nombre debe corresponderse al tamaño de su ámbito. Así, si una variable tiene un ámbito muy reducido, se puede usar un nombre ultra corto, por ejemplo: `for (int i=0; i<10 i++)`, esa variable `i` sólo existe dentro del for.
- [ ] El nombre de las funciones siempre es una acción, por tanto se usan verbos. Ser consistente y evitar cosas como: obtenerCiudad, recuperarProvincia, extraerPais. Las 3 deberían usar el mismo verbo si las 3 "obtienen o recuperan o extraen" cosas.
- [ ] El nombre de las clases siempre es un sustantivo, y siempre deben estar en singular.

### Estructura de una ETL

- [ ] El programa principal de la ETL debe contener los pasos a alto nivel de lo que hace la etl. Debe ser claro y no complicarlo

##### Ejemplo a alto nivel

```
flow = FlowControl

def main():
    try:
        obtener_ficheros
        loop // por cada fichero
        success = procesar_fichero (file)
    end loop
    except Exception:
        flow.handle_error(err.message, True)
    return true

def procesar_fichero (file)
    success = true
    try:
        validar_nombre_fichero
        leer_fichero
        validar_datos
        enviar_iotagent
        mover_ficheros_procesados
    except Exception
        gestion_errores
        flow.handle_error(err.message, False)
    finally
        eliminar_ficheros_temporales
    return success
```

### Programa principal o main

- [ ] El programa principal se encuentra en un fichero "etl.py"
- [ ] El programa principal se encuentra en una función "main"
- [ ] La función "main" siempre devuelve true si ha ido bien, para que Jenkins muestre una bolita verde como que el proceso ha acabado bien
- [ ] La función "main" debe tener un único try - except, y dentro del except se debe usar la clase FlowControl de la librería vlciShared y llamar al método handle_error pasando como 2o parámetro un True, para que el proceso acabe con error y Jenkins muestre una bolita roja.

### Variables globales

- [ ] Está prohibido su uso

### Funciones

- [ ] Las funciones hacen una cosa, no muchas
- [ ] Las funciones deben restringir el número de parámetros. A lo sumo, tres parámetros de entrada. [Leer la explicación completa aquí](https://matheus.ro/2018/01/29/clean-code-avoid-many-arguments-functions/). Si se necesitan más parámetros replantearse otras opciones:
  - [ ] Quizás la función no hace sólo una cosa.
  - [ ] Quizás los parámetros se pueden modelizar en forma de objeto.

### Clases

- [ ] Se recomienda fuertemente empaquetar datos y funcionalidades juntos en forma de clases
- [ ] Establecer elementos privados y públicos de las clases

### Errores

- [ ] El programa principal debe ser el responsable de tratar cualquier error. Esto implica que, en lugar de dejar que los errores se propaguen a lo largo del programa sin control, el código principal debe tener estructuras de control adecuadas para capturar, identificar y manejar los errores de manera apropiada, pero teniendo en cuenta al mismo tiempo que "el programa principal debe tener un único try - except"
- [ ] Si por cualquier motivo se captura una excepción en cualquier punto del programa y se relanza (debe haber un buen motivo para hacerlo), en la nueva excepción que se relance inyectar los datos de error de la excepción original.
- [ ] Si la etl falla de forma controlada, siempre se debe hacer un sys.exit(VALUE), donde VALUE será diferente a 0 (por ejemplo 1). De esta forma, en Jenkins se verá el JOB como fallido (bola roja)
- [ ] Si la etl falla de forma controlada, en los logs se explicará detalladamente el problema y en la medida de lo posible instrucciones para resolver el problema. Ejemplo: si hay un error en un formato de una fecha, indicar la linea dónde ha sucedido el problema y qué hacer cuando eso pase (como ejemplo): contactar con el sistema origen para que corrijan el dato y una vez el dato esté corregido, borrar los datos insertados en esa ejecución fallida y relanzar la etl.
- Finalmente, hay varios tipos de errores:

  - TIPO 1 - `Errores de sistema`. Son siempre fatales. No se deben capturar en las partes internas de la etl. Se capturarán en el programa principal y se hará una salida con error mediante el método handle_error.
  - TIPO 2 - `Errores de aplicación fatales`. Son errores generados por nosotros y que deben parar la ejecución de la etl porque son problemas graves que impiden continuar. Para ello, llamar al método handle_error de la clase FlowControl de nuestra librería VLCIShared, con el segundo parámetro a True, que indica que es un error fatal y se debe abortar.
  - TIPO 3 - `Errores de aplicación no fatales, pero que implican ETL KO/fallida`. Son errores generados por nosotros y que no paran la ejecución de la etl completamente sino que saltan una parte del proceso. Por ejemplo, cuando falla la validación de una fila, pero queremos seguir procesando otra fila. Se hace lanzando excepciones built-in de Python y al capturarla, usar el método handle_error de la clase FlowControl de nuestra librería VLCIShared, con el segundo parámetro a False, que indica que es un error no fatal, por tanto no se aborta, pero la ETL debe acabar al final con error.
  - TIPO 4 - `Errores de aplicación no fatales y además la ETL no debe fallar`. Esto es un escenario muy poco o nada habitual. Si se da, no hace falta usar la clase FlowControl. Se puede tratar simplemente con excepciones built-in de Python, capturándolas cuando sucedan y gestionándolas.

  - [ ] Por tanto, sólo las excepciones/errores de TIPO 4 admiten un try-except donde no se use el método handle_error de la clase FlowControl. Todos los demás try-except deben usar el método handle_error de la clase FlowControl.

### Logs

- [ ] Tanto en caso de error como de éxito, la ETL es conveniente que muestre al final un resumen de los números de registros tratados (tanto leídos, como procesados, como con error en caso de error). Mensajes de ejemplo de registros leídos: "Se han leido 4.135 registros del fichero de entrada". Mensajes de ejemplo de registros procesados: "Se han insertado en bbdd 3.553 registros". Mensajes de ejemplo de registros con error: "Han fallado 78 registros".
- [ ] Todos los logs (de error o de info), deben estar en castellano

### Comentarios

- [ ] Se han de añadir comentarios a las funciones públicas (no las privadas) explicando qué hacen, sus parámetros de entrada y la potencial salida.
  - En el apartado de Unit Testing se explica qué son funciones públicas y privadas.
- [ ] No abusar de comentarios. Poner comentarios cuando el código no se entienda por sí mismo.

### Configuración de una ETL

Se tendrán 3 tipos de configuraciones:

- Datos sensibles (secrets) o específicos de entorno y globales al proyecto VLCi y por tanto comunes a todas las ETLs. Ejemplos:
  - El HOST de una base de datos: no es sensible, pero sí variable para LOCAL, PRE o PRO
  - El USER o PASSWD del servidor SFTP son datos sensibles
- Datos sensibles (secrets) o específicos de entorno y específicos de una ETL. Ejemplos:
  - El APIKEY con el que conectamos con el IoT Agent de plataforma esa ETL es un dato sensible
  - El absolute path donde se ubica un fichero, si por ejemplo varía en función de si estoy en LOCAL, PRE o PRO
- Datos no sensibles y no variables entre entornos, pero que pueden variar con el tiempo. Valores de configuración de la ETL invariable del entorno en el que nos encontremos. Ejemplos:
  - El nombre de un fichero que debo leer. Que siempre será el mismo independientemente del entorno en el que esté.
  - El número máximo de reintentos de un proceso antes de fallar. Y es un valor que no queremos que varíe entre los diferentes entornos.

Hay un cuarto elemento, que no se puede considerar de configuración, pero que se explica en este ticket por claridad. Este cuarto tipo sería lo que habitualmente se conoce también como CONSTANTES, que las tendremos configuradas dentro de la propia ETL en un fichero de constantes. Ejemplos:

- MILISEGUNDOS = 1000
- PI = 3.1415
- CODIGO_ERROR_NOT_FOUND = 404

En cualquier caso, los 4 tipos siguen siempre la misma nomenclatura: UPPER_SNAKE_CASE.

## Creación de un proyecto ETL Python

La información sobre cómo prepararse el PC para empezar a trabajar con ETLs Python se puede encontrar en el Sharepoint del proyecto, carpeta HowTo.  
[HowTo - Entornos - Guía para el entorno local de Python para ETLs.docx](https://gohub.sharepoint.com/:w:/r/sites/ValenciaSmartCity/desarrolloVLCi/Documentos%20compartidos/10.%20HowTo/HowTo%20-%20Entornos%20-%20Gu%C3%ADa%20para%20el%20entorno%20local%20de%20Python%20para%20ETLs.docx?d=wf245f0705fd146fda58352d4debbb01e&csf=1&web=1&e=FrUXeC)

## Testing

Es obligatorio que las etls tengan test automático con PyTest, tanto test unitario de funciones como test de componente (toda la etl vista como una caja negra, validando entradas y salidas).

### Unit Testing

- [ ] Los tests unitarios se deben realizar sobre funciones/métodos públicos y no es necesario hacerlo sobre las funciones/métodos privados, ya que se supone que las funciones públicas internamente harán uso de las funciones privadas y por tanto indirectamente estamos probando esas funciones privadas.
  - En Python, si no usamos clases y lo que tenemos son funciones repartidas entre ficheros, se entiende una función como pública aquella que usamos desde otro fichero
- [ ] Un test unitario puede usar varios datasets para validar entradas/salidas. [Más info sobre esto aquí](https://docs.pytest.org/en/stable/how-to/parametrize.html)

![alt text](unittest_severaldatasets.png)

# Buenas prácticas en las ETLs de Talend

## Cómo usar y nombrar las variables globales a todas las ETLs Talend

En el properties TSOL global se incluirán las variables globales de conexión como por ejemplo:

- Host, port, user, password, de una base de datos
- Host, port, del SMTP del envío de emails.

¿Qué no irá al TSOL global? Valores concretos de la ETL. Ej:

- Subject del email
- Nombres de fichero / extensiones

Qué nomenclatura seguiremos para nombrar las variables globales del TSOL:
[global].[tipo].[marca].[valor]

Ej:

- global.database.sqlserver.host
- global.smtp.port
